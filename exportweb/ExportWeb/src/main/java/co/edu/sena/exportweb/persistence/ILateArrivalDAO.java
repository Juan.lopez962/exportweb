/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package co.edu.sena.exportweb.persistence;

import co.edu.sena.exportweb.model.ApprenticeCount;
import co.edu.sena.exportweb.model.LateArrival;
import java.util.Date;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Aprendiz
 */
@Local
public interface ILateArrivalDAO {
    public LateArrival findById(Integer id) throws Exception;
    public List<LateArrival> findAll() throws Exception;
    public List<LateArrival> findByDateRange(Date date1, Date date2) throws Exception;
    public List<ApprenticeCount> findGroupByApprentice() throws Exception;
}
