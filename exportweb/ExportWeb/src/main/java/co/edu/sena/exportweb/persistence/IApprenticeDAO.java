/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package co.edu.sena.exportweb.persistence;

import co.edu.sena.exportweb.model.Apprentice;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Aprendiz
 */
@Local
public interface IApprenticeDAO {
    public Apprentice findById(Long document) throws Exception;
    public List<Apprentice> findAll() throws Exception;
}
